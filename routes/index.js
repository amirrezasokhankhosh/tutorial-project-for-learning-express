var express = require('express');
var router = express.Router();

var queries = require('../db/queries');
const { route } = require('./users');

// MIDDLE FUNCTIONS

function isValidId(req , res , next){
  if(!isNaN(req.params.id)){
    return next();
  }
  else {
    next(new Error('Invalid id!'));
  }
}

function isValidSticker(sticker){
  const hasTitle = typeof sticker.title == 'string' && sticker.title.trim() != '';
  const hasUrl = typeof sticker.url == 'string' && sticker.url.trim() != '';
  return hasTitle && hasUrl;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

// GET ALL THE STICKERS
router.get('/stickers' , function(req , res , next){
	queries.getAll().then(stickers => {
		context = {stickers : stickers};
		res.render('stickers' , context);
	});
});

// GET A STICKER WITH THE ID IN URL
router.get('/sticker/:id' , isValidId , function(req , res , next){
  queries.getOne(req.params.id).then(sticker => {
    if (sticker){
	  context = {sticker : sticker};
	  res.render('sticker' , context);
    } else {
      res.status(404)
      next(new Error("Not Found!"));
    }
  });
});

// GET THE NEW STICKER TEMPLATE
router.get('/new_sticker' , function(req , res , next){
  res.render('new_sticker');
});

// SAVE THE NEW STICKER
router.post('/new_sticker_save' , function(req , res , next){
  if(isValidSticker(req.body)){
    queries.create(req.body).then(() => {
      res.redirect('/stickers');
    })
  } else {
    res.status(404)
    next(new Error("Sorry, There was something wrong with you form!"));
  }
});

// GET THE UPDATE STICKER TEMPLATE 
router.get('/update_sticker/:id', isValidId, function(req, res, next){
  queries.getOne(req.params.id).then(sticker => {
    if (sticker){
      context = {id : req.params.id ,sticker : sticker};
      res.render('update_sticker' , context);
    } else {
      res.status(404)
      next(new Error("Not Found!"));
    }
  });
});

// SAVE CHANGES OF A STICKER
router.post('/update_sticker_save/:id' , isValidId , function(req , res , next){
	if(isValidSticker(req.body)){
		queries.update(req.params.id , req.body).then(() =>{
			res.redirect('/stickers');
		})
	} else {
		res.status(404)
    	next(new Error("Sorry, There was something wrong with you form!"));
	}
});

// DELETE A STICKER
router.get('/delete_sticker/:id' , isValidId , function(req , res , next){
	queries.delete(req.params.id).then(() => {
		res.redirect('/stickers');
	})
});

module.exports = router;
